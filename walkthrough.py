board = [
    [7,8,0,4,0,0,1,2,0], # test
    [6,0,0,0,7,5,0,0,9],
    [0,0,0,6,0,1,0,7,8],
    [0,0,7,0,4,0,2,6,0],
    [0,0,1,0,5,0,9,3,0],
    [9,0,4,0,6,0,0,0,5],
    [0,7,0,3,0,0,0,1,2],
    [1,2,0,0,0,7,4,0,0],
    [0,4,9,2,0,6,0,0,7]
]
# SUDOKU RULES
# Numbers 1-9 in a line both ways, only one instance of 1-9 in each 3x3 box


# 1. Pick empty square
# 2. Try all numbers
# 3. Find one that works
# 4. Repeat
# 5. Backtrack

# Create the ability to print the board above
def print_board(board):
    for y in range(len(board)): # i = row number here
        if y % 3 == 0 and y != 0: # if the row number / 3 leaves no remainder | != so it doesn't print the - - - at the top
            print("- - - - - - - - - - - -")

        for x in range(len(board[0])): # this will give the # of columns
            if x % 3 == 0 and x != 0: # num columns % 3 == 0 print |
                # remember x starts at 0, not 1
                print(" | ", end="") # end will keep from printing on new line after the |

            if x == 8: # for the edge case at the end of the board (column wise)
                print(str(board[y][x])) # It will print JUST the number at that y,x
                # we have to do board y,x because the row is the actual list number, and the x value is the index within that imbedded list
            else:
                print(str(board[y][x]) + " ", end="") # this is the actual main print


def find_empty(board):
    for y in range(len(board)): # sets the rows/object value to y
        for x in range(len(board[0])): # sets columns value to x
            if board[y][x] == 0: # if the y,x coordinate is 0
                return (y, x) # row, col <-- where there is an empty
    return False # if no position where 0, then returns False, used later
        



# Creating a function that checks if the new number we are about to insert
# into the board is a valid number.
# Takes in 3 parameters, the board, the number to be inserted, and the position
# Position is (row, col)
def valid(board, num, pos):

    # Check row --> checks all the same y --> checks all the same board[object] (row)
    for x in range(len(board[0])):
        if board[pos[0]][x] == num and pos[1] != x: # the pos[1] is the col val
            # the col != x so we don't check the number we just inserted
            return False
    
    # Check column (vertically) --> checks all the same x
    for y in range(len(board)):
        if board[pos[1]][y] == num and pos[0] != y:
            return False

    # Check box
    # First declare which box the position is in
    box_x = pos[1] // 3 # pos[1] is the x value (col) // 3 --> tells you box 0-2 across
    box_y = pos[0] // 3 # same idea but with y value (row) --> tells box 0-2 down
    # box (0,0) | box (0,1) | box(0,2)
    # box (1,0) | box (1,1) | box(1,2)
    # box (2,0) | box (2,1) | box(2,2)

    for y in range(box_y * 3, box_y*3 + 3): # Uses which box we're in to find range
        for x in range(box_x * 3, box_x*3 + 3): # +3 is there to read all 3 nums in box
            if board[y][x] == num and (y,x) != pos: # if coord == new num-> false
                # if the above and y,x that is checked isn't the pos ->false
                return False

    return True

def solve_board(board): # Done through recurssion -- call the function inside itself
    
    find = find_empty(board) # Returns a tuple (y,x) or False
    if find == False: # no empty points in board
        return True 
    else: # empty nums in board -> do algorithim
        row, col = find # func-find_empty returns 2 values, we declare row, col to that
    for i in range(1,10): # Cannot use 0 (empty) and range counts up to 10, (1-9)
        if valid(board, i, (row, col)): # func-valid has 3 parameters, the pos param has two parts: row, col
            board[row][col] = i # declares that the coordinate [row][col] = num i

            if solve_board(board): # Recurssion, do the program again
                return True

            # if the number input fails, return it to an empty value before continuing
            board[row][col] = 0 
    return False





print_board(board)
print(find_empty(board))